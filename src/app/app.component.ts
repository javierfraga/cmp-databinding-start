import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  serverElements = [{
    type: 'server',
    name: 'Testserver',
    content: 'Just a test'
  }];

  test:boolean = true;
  testMe() {
    this.test = !this.test; 
  }

  addServer(serverData:{serverName:string,serverContent:string}) {
    this.serverElements.push({
      type: 'server',
      name: serverData.serverName,
      content: serverData.serverContent
    });
  }

  addBlueprint(bluePrintData:{serverName:string,serverContent:string}) {
    this.serverElements.push({
      type: 'blueprint',
      name: bluePrintData.serverName,
      content: bluePrintData.serverContent
    });
  }
l
	changeFirst() {
		this.serverElements[0].name = 'Changed';
	}
	destroyFirst() {
		this.serverElements.splice(0,1);
	}
}
