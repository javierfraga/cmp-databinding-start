import { Component, OnInit, EventEmitter, Output, ViewChild,ElementRef } from '@angular/core';

@Component({
  selector: 'app-cockpit',
  templateUrl: './cockpit.component.html',
  styleUrls: ['./cockpit.component.css']
})
export class CockpitComponent implements OnInit {
  @Output() onServerCreated = new EventEmitter<{serverName:string, serverContent:string}>();
  @Output('onBpCreated') someInternalName = new EventEmitter<{serverName:string, serverContent:string}>();
  // newServerName = '';
  // newServerContent = '';
  // Is not recommended to use this method use directives instead
  // should use string interpolation or property binding if want access DOM
  @ViewChild('serverContentInput') serverContentInput:ElementRef; // shows how we can access dom template variabel

  addServer(serverNameInput:HTMLInputElement) {
    // console.log("serverNameInput", serverNameInput.value);
    // console.log(this.serverContentInput);
    this.onServerCreated.emit(
      {
        serverName: serverNameInput.value,
        // nativeElement is soo ugly
        serverContent: this.serverContentInput.nativeElement.value
      }
    ); // shows how we can access dom template variabel
  }

  addBlueprint(serverNameInput:HTMLInputElement) {
     this.someInternalName.emit(
     {
      serverName: serverNameInput.value,
        // nativeElement is soo ugly
      serverContent: this.serverContentInput.nativeElement.value
     }
    ); // shows how we can access dom template variabel
  }

  constructor() { }

  ngOnInit() {
  }

}
