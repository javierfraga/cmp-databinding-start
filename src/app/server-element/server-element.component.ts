import { 
	Component, 
	OnInit, 
	Input, 
	ViewEncapsulation,
	OnChanges,
	DoCheck,
	AfterContentInit,
	AfterContentChecked,
	AfterViewChecked,
	AfterViewInit,	
	OnDestroy,
	SimpleChanges,
	ViewChild,
	ElementRef,
	ContentChild,
} from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-server-element',
  templateUrl: './server-element.component.html',
  styleUrls: ['./server-element.component.css'],
  encapsulation: ViewEncapsulation.Emulated // Emulated Shadow DOM for broswer support, on by default, this is redundant
  // encapsulation: ViewEncapsulation.None // Turn off CSS encasulation
  // encapsulation: ViewEncapsulation.Native // Turn on Shadow DOM native support, careful browser may not support yet
  })
export class ServerElementComponent implements 
OnInit, 
OnChanges, 
DoCheck, 
AfterContentChecked,
AfterViewChecked,
AfterViewInit,
OnDestroy,
AfterContentInit
{
	@Input('srvElement') element:{type:string, name:string, content:string};
	@Input() name:string;
	@ViewChild('heading') heading:ElementRef;
	@ContentChild('contentParagraph') contentParagraph:ElementRef;
	@ViewChild('viewChildText') viewChildText:ElementRef;
	observableText = new FormControl();
	constructor() {
		console.log('00 Constructor');
		this.observableText.valueChanges.subscribe(
			(value)=> console.log(value)
		);
	}

	/*
	 * only this one has SimpleChnages and they are the variable that are @Input()
	 */
	ngOnChanges(changes:SimpleChanges) {
		console.log("*********************************");
		console.log('01 ngOnChanges: Executed at start when component is created but also anytime a variable changes that is linked to @Input()');
		console.log(changes);
	}
	ngOnInit() {
		console.log('02 ngOnInit: Executed when Angular is done creating the element but it has not yet been added to DOM but properties can be accessed. This runs after the constructor.');
		console.log("this will be empty",this.heading.nativeElement.textContent)
		console.log("@ContentChild(): doesn't exist yet at this lifecycle: ",this.contentParagraph.nativeElement.textContent)
	}
	ngDoCheck() {
		console.log('03 ngDoCheck: Executed anytime there is a change in the component.');
	}
	ngAfterContentInit() {
		console.log('04 ngAfterContentInit: Executed after is done adding content with ng-content from parent component that inject target component');	
		console.log("@ContentChild(): now exists in this lifecycle: ",this.contentParagraph.nativeElement.textContent)
	}
	ngAfterContentChecked() {
		console.log('05 ngAfterContentChecked: Executed when a change is detected in that content added to ng-content from parent');	
	}
	ngAfterViewInit() {
		console.log('06 ngAfterViewInit: Executed when the view is constructed and rendered');	
		console.log(this.heading.nativeElement.textContent)
	}
	ngAfterViewChecked() {
		console.log('07 ngAfterViewChecked: Executed anytime it checks if changes to the view have been made');	
		console.log(this.heading.nativeElement.textContent)
		console.log("------------------------------------")
	}
	ngOnDestroy() {
		console.log('08 ngOnDestroy; Executed when removed from DOM');	
	}

	consoleLogText(){
		console.log(this.viewChildText.nativeElement.value);
	}
}
