import { Component, Directive, Input, ViewChild } from '@angular/core';
import { Pane } from '../content-child/content-child.component';

@Component({
	selector: 'app-view-child',
	template: `
	<pane id="1" *ngIf="shouldShow"></pane>
	<pane id="2" *ngIf="!shouldShow"></pane>

	<button (click)="toggle()">Toggle</button>

	<div>Selected: {{selectedPane}}</div> 
	`,
	styleUrls: ['./view-child.component.css']
})
export class ViewChildComponent {

	// this is where accesses "pane" in template above
	// notice the signature syntax
	// @ViewChild() is used at its component template level and targets first
	// element or directive matching this name
	@ViewChild(Pane) set pane(v: Pane) {
		setTimeout(() => { this.selectedPane = v.id; }, 1000);
	}
	selectedPane: string = '';
	shouldShow = true;
	toggle() { this.shouldShow = !this.shouldShow; }

}
